## Telegram bot delivering audibooks as voice messages.

## Use:

- can be hosted on a free heroku dyno instance 
- uses Dropbox api to retrieve the files from a dropbox folder
- requires environment vairables: `PORT`, `TELEGRAM_TOKEN`, `DROPBOX_TOKEN`

![image](mockups_sciphil_bot.png)