from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackQueryHandler
from telegram import InlineKeyboardButton, ReplyKeyboardMarkup, InlineKeyboardMarkup
import logging
import os
import dropbox

def get_flist(dbx, path):
    flist = list()
    list_of_folders = dbx.files_list_folder(path).entries
    for file in list_of_folders: flist.append(file.name)
    return flist



def build_menu(buttons,
               n_cols,
               header_buttons=None,
               footer_buttons=None):
    menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
    if header_buttons:
        menu.insert(0, header_buttons)
    if footer_buttons:
        menu.append(footer_buttons)
    return menu

def act_on_message(bot, update, dbx):
    text=update.message.text
    list_of_folders = get_flist(dbx, "/Sciphil mp3")
    splitted_text = text.split("/")
    print(splitted_text)
    if "mp3" in text and splitted_text[1] in get_flist(dbx,'/SciPhil mp3/' + splitted_text[0])  :
        print(splitted_text[1])
        update.message.reply_text("uploading the file... (may take a while) ")
        dbx.files_download_to_file(splitted_text[1],"/SciPhil mp3/"+text)
        file = open(splitted_text[1], 'rb')
        bot.send_audio(chat_id=update.message.chat_id, audio=file, timeout = 1000)
        file.close()
        if os.path.isfile(splitted_text[1]): os.remove(splitted_text[1])
        else: print("error, could not delete file")
    elif text in list_of_folders:
        print("in")
        list_of_folders = get_flist(dbx,'/SciPhil mp3/'+text)
        button_list = list()
        button_list.append(InlineKeyboardButton("/back", callback_data = print("back")))
        for name in list_of_folders:
            if ".mp3" in name: button_list.append(InlineKeyboardButton(text+"/"+name, callback_data= print("whatever")))
        reply_markup = ReplyKeyboardMarkup(build_menu(button_list, n_cols=1))
        bot.send_message(chat_id = update.message.chat_id, text = "Choose a recording", reply_markup=reply_markup)
    else:
        update.message.reply_text("don't know what to do")

def show_menu(bot, update, dbx):
    list_of_folders = get_flist(dbx,'/SciPhil mp3')
    button_list = list()
    for name in list_of_folders:
        if "roblem" in name:
            button_list.append(InlineKeyboardButton(name, callback_data= name))
    reply_markup = ReplyKeyboardMarkup(build_menu(button_list, n_cols=1))
    bot.send_message(chat_id = update.message.chat_id, text = "Choose a problem", reply_markup=reply_markup)
    return 0


def pay(bot, update):
    keyboard = [[InlineKeyboardButton("Donate", url="paypal.me/philosophyofsciencek", callback = '1'),]]
    reply_markup = InlineKeyboardMarkup(keyboard)
    update.message.reply_text('Please donate 2€, thank you very much in advance', reply_markup=reply_markup)

def help(bot, update):
    update.message.reply_text("Use /start to test this bot.")

def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)

DROPBOX_TOKEN = os.environ.get('DROPBOX_TOKEN')
dbx = dropbox.Dropbox(DROPBOX_TOKEN)
start_lam = lambda bot, update: show_menu(bot, update, dbx)
lact_on_message = lambda bot, update: act_on_message(bot, update, dbx)

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)
TOKEN = os.environ.get('TOKEN')
NAME = "sciphil-bot"
PORT = os.environ.get('PORT')
updater = Updater(TOKEN)
dp = updater.dispatcher
dp.add_handler(CommandHandler("start", start_lam))
dp.add_error_handler(error)
dp.add_handler(CommandHandler("back", start_lam))
message_handler = MessageHandler(Filters.text, lact_on_message)
dp.add_handler(message_handler)
dp.add_handler(CommandHandler('help', help))
dp.add_handler(CommandHandler("pay", pay))
#dp.add_handler(CallbackQueryHandler(button))
updater.start_webhook(listen="0.0.0.0",
                      port=int(PORT),
                      url_path=TOKEN)
updater.bot.setWebhook("https://{}.herokuapp.com/{}".format(NAME, TOKEN))
updater.idle()
